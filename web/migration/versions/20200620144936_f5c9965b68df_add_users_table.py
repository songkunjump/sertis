"""add users table

Revision ID: f5c9965b68df
Revises: 
Create Date: 2020-06-20 14:49:36.056403

"""
from alembic import op
from sqlalchemy import String, Column, Boolean, TIMESTAMP, ForeignKey, Integer
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.sql import func


# revision identifiers, used by Alembic.
revision = "f5c9965b68df"
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "cards",
        Column('id', UUID(as_uuid=True), nullable=False, primary_key=True),
        Column("name", String(255), nullable=False),
        Column("status", String(255), nullable=False),
        Column("content", String(255), nullable=False),
        Column("category", String(255), nullable=False),
        Column("username", String(255), nullable=False),
        Column("password", String(255), nullable=False),
        
    
    )


def downgrade():
    op.drop_table("cards")
