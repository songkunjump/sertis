import logging
import os

from logging import handlers
from pathlib import Path


# python logging disable stack trace (https://stackoverflow.com/a/54605728)
class TracebackInfoFilter(logging.Filter):
    """Clear or restore the exception on log records"""

    def __init__(self, clear=True):
        self.clear = clear

    def filter(self, record):
        if self.clear:
            record._exc_info_hidden, record.exc_info = record.exc_info, None
            # clear the exception traceback text cache, if created.
            record.exc_text = None
        elif hasattr(record, "_exc_info_hidden"):
            record.exc_info = record._exc_info_hidden
            del record._exc_info_hidden
        return True


def getLogger(name=None):
    logger = logging.getLogger(name)
    return logger


def getLoggerForHandlers(name=None):
    logger = getLogger(name)
    logger.setLevel(logging.INFO)

    log_directory = f"{Path().absolute()}/logs"
    log_filename = "handlers.log"
    log_path = os.path.join(log_directory, log_filename)
    Path(log_directory).mkdir(parents=True, exist_ok=True)

    handler = handlers.TimedRotatingFileHandler(
        filename=log_path, when="midnight", interval=1, utc=True
    )
    handler.suffix = "%Y-%m-%d"
    handler.setLevel(logging.INFO)

    formatter = logging.Formatter("%(asctime)s %(name)s %(levelname)s %(message)s")

    handler.setFormatter(formatter)
    handler.addFilter(TracebackInfoFilter())
    logger.addHandler(handler)
    return logger
