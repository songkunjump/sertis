from __future__ import annotations
import abc

from api.domain import model





class AbstractCardRepository(abc.ABC):
    def __init__(self):
        pass

    def get(self, name: str) -> model.Card:
        card = self._get(name)
        if card:
            return card

    def create(self, card: Card):
        self._create(card)

    @abc.abstractmethod
    def _get(self, name: str) -> model.Card:
        raise NotImplementedError

    @abc.abstractmethod
    def _create(self, card: Card):
        raise NotImplementedError


class CardRepository(AbstractCardRepository):
    def __init__(self, session):
        super().__init__()
        self.session = session

    def _get(self, name: str):

        return self.session.query(model.Card).filter_by(name=name).first()

    def _create(self, card: model.Card):
        return self.session.add(card)


