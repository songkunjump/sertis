from sqlalchemy import (
    Table,
    MetaData,
    Column,
    Integer,
    String,
    ForeignKey,
    Boolean,
    TIMESTAMP,
    event,
    PrimaryKeyConstraint,
)
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.orm import mapper

from api import logging
from api.domain import model

logger = logging.getLogger(__name__)

metadata = MetaData()



cards = Table(
    "cards",
    metadata,
    Column("id", UUID(as_uuid=True), primary_key=True),
    Column("name", String(255), nullable=False),
    Column("status", String(255), nullable=False),
    Column("content", String(255), nullable=False),
    Column("category", String(255), nullable=False),
    Column("username", String(255), nullable=False),
    Column("password", String(255), nullable=False),
    
    
    
    

)



# not sure about mapper
def start_mappers():
    logger.info("Starting mappers")
    cards_mapper = mapper(
        model.Card,
        cards,
    )

    
   


@event.listens_for(model.Card, "load")
def receive_user_load(card, _):
    card.events = []
