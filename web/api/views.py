import sys
sys.path.append("..")
from api.service import unit_of_work




def get_card(name: int, uow: unit_of_work.SqlAlchemyUnitOfWork):
    with uow:
        result = uow.session.execute(
            "SELECT name,status,content,category,username FROM cards WHERE name = :name", dict(name=name)
        )
        role = next(result, None)
        return dict(role) if role else None
