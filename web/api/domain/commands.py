from dataclasses import  dataclass



class Command:
    pass
    

@dataclass
class CreateUser(Command):
    username: str
    password: str

@dataclass
class CreateCard(Command):
    name: str
    status: str
    content: str
    category: str
    username: str
    password: str
    

@dataclass
class UpdateName(Command):
    name: str
    new_name:str
    username: str
    password: str

@dataclass
class UpdateStatus(Command):
    name: str
    status: str
    username: str
    password: str

@dataclass
class UpdateContent(Command):
    name: str
    content: str
    username: str
    password: str

@dataclass
class UpdateCategory(Command):
    name: str
    category: str
    username: str
    password: str

@dataclass
class RemoveCard(Command):
    
    name: str
    username: str
    password: str

