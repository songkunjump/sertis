from __future__ import annotations

import uuid
from api.utils import random_integer, random_string
from api.domain import events
from sqlalchemy.dialects.postgresql import UUID





class Card:

    def __init__(self, name: str, status: str, content: str, category: str,username:str,password:str):
        self.id = str(uuid.uuid4())
        self.name = name 
        self.status = status
        self.content = content
        self.category = category
        self.username = username
        self.password = password
        self.events = []
    
    
    def check(self, username: str, password : str):
        if username == self.username and password == self.password:
            return True
        else:
            return False
    def update_name(self, name: str):
        
        self.name = name
        

    def update_status(self, status: str):
        
        self.status = status
    
    def update_content(self, content: str):
        
        self.content = content
    
    def update_category(self, category: str):
        
        self.category = category
        



