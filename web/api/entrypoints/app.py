

from flask import Flask, request, make_response, jsonify



from api import bootstrap, config, views
from api.domain import commands
from api.service import exceptions



app = Flask(__name__)
bus = bootstrap.bootstrap()





@app.route("/create_card", methods=["POST"])
def create_card():
    name = request.json.get("name")
    status = request.json.get("status")
    content = request.json.get("content")
    category = request.json.get("category")
    username = request.json.get("username")
    password = request.json.get("password")
   

    if name and status and content and category and username and password:
        try:

            cmd = commands.CreateCard(name,status,content,category,username,password)

            bus.handle(cmd)

            # role = views.get_role(cmd.id, bus.uow)
            response = make_response()
            response.status_code = 201

            return response
        except exceptions.DuplicatedCard as e:
            return jsonify(e.messages), 400

    else:
        return "Bad Request", 400

@app.route("/update_name/<name>", methods=["POST"])
def update_name(name):

   
    username = request.json.get("username")
    password = request.json.get("password")
    new_name = request.json.get("new_name")
   

    if name and new_name and username and password:
        try:

            cmd = commands.UpdateName(name,new_name,username,password)

            bus.handle(cmd)

        
            response = make_response()
            response.status_code = 200

            return response
        except exceptions.UnauthorizedError as e:
            return jsonify(e.messages), 400
        except exceptions.CardNotFoundError as e:
            return jsonify(e.messages), 404

    else:
   
         return "Bad Request", 400
@app.route("/update_status/<name>", methods=["POST"])
def update_status(name):
    
    status = request.json.get("status")
    username = request.json.get("username")
    password = request.json.get("password")

   

    if  name and status and username and password:
        try:

            cmd = commands.UpdateStatus(name,status,username,password)

            bus.handle(cmd)

            # role = views.get_role(cmd.id, bus.uow)
            response = make_response()
            response.status_code = 200

            return response
        except exceptions.UnauthorizedError as e:
            return jsonify(e.messages), 400
        except exceptions.CardNotFoundError as e:
            return jsonify(e.messages), 404
    else:
   
         return "Bad Request", 400

@app.route("/update_content/<name>", methods=["POST"])
def update_content(name):
   
    content = request.json.get("content")
   
    username = request.json.get("username")
    password = request.json.get("password")

   

    if name  and content and username and password:
        try:

            cmd = commands.UpdateContent(name,content,username,password)

            bus.handle(cmd)

            # role = views.get_role(cmd.id, bus.uow)
            response = make_response()
            response.status_code = 200

            return response
        except exceptions.UnauthorizedError as e:
            return jsonify(e.messages), 400
        except exceptions.CardNotFoundError as e:
            return jsonify(e.messages), 404

    else:
   
         return "Bad Request", 400

@app.route("/update_category/<name>", methods=["POST"])
def update_category(name):
    
    category = request.json.get("category")
   
    username = request.json.get("username")
    password = request.json.get("password")

   

    if  name and category and username and password:
        try:

            cmd = commands.UpdateCategory(name,category,username,password)

            bus.handle(cmd)

            # role = views.get_role(cmd.id, bus.uow)
            response = make_response()
            response.status_code = 200

            return response
        except exceptions.UnauthorizedError as e:
            return jsonify(e.messages), 400
        except exceptions.CardNotFoundError as e:
            return jsonify(e.messages), 404

    else:
   
         return "Bad Request", 400

@app.route("/remove_card/<name>", methods=["POST"])
def remove_card(name):
    
    
   
    username = request.json.get("username")
    password = request.json.get("password")

   

    if  name and username and password:
        try:

            cmd = commands.RemoveCard(name,username,password)

            bus.handle(cmd)

            # role = views.get_role(cmd.id, bus.uow)
            response = make_response()
            response.status_code = 200

            return response
        except exceptions.UnauthorizedError as e:
            return jsonify(e.messages), 400
        except exceptions.CardNotFoundError as e:
            return jsonify(e.messages), 404

    else:
   
         return "Bad Request", 400



@app.route("/article/<name>", methods=['GET'])
def get_article(name):
    result = views.get_card(name,bus.uow)
    if not result:
        return 'not found', 404
    return jsonify(result), 200





if __name__ == "__main__":
    app.run(host="0.0.0.0", port=92, debug=True)
