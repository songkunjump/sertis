import string
import random


def random_string():
    return "".join(
        random.SystemRandom().choice(string.ascii_uppercase + string.digits)
        for _ in range(random.randint(1, 10))
    )


def random_integer():
    return random.randint(1, 1000)
