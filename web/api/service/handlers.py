from __future__ import annotations
from typing import TYPE_CHECKING


from api import utils
from api.domain import commands, events, model
from api.service import exceptions




def create_card(cmd: commands.CreateCard, uow: unit_of_work.AbstractUnitOfWork):
    with uow:

        card = uow.cards.get(name=cmd.name)
        if card is not None:
            raise exceptions.DuplicatedCard

        card = model.Card(
            name=cmd.name,
            status=cmd.status,
            content=cmd.content,
            category=cmd.category,
            username=cmd.username,
            password=cmd.password,
            
        )

        
        uow.cards.create(card)
        uow.commit()


def update_name(cmd: commands.UpdateName, uow: unit_of_work.AbstractUnitOfWork):
    with uow:
        card = uow.cards.get(name=cmd.name)

        if card == None:
            raise exceptions.CardNotFoundError
        if not card.check(cmd.username,cmd.password) :
            raise exceptions.UnauthorizedError


        card.update_name(cmd.new_name)
        uow.session.execute(
            'UPDATE cards SET name = :new_name WHERE id = :id',
            
            dict(id=card.id,new_name=cmd.new_name)
        )
        
        uow.commit()

def update_status(cmd: commands.UpdateStatus, uow: unit_of_work.AbstractUnitOfWork):
    with uow:

        card = uow.cards.get(name=cmd.name)

        if card == None:
            raise exceptions.CardNotFoundError
        if not card.check(cmd.username,cmd.password) :
            raise exceptions.UnauthorizedError


        card.update_status(cmd.status)
        uow.session.execute(
            'UPDATE cards SET status = :status WHERE name= :name',
            
            dict(name=cmd.name,status=cmd.status)
        )
        uow.commit()

def update_content(cmd: commands.UpdateContent, uow: unit_of_work.AbstractUnitOfWork):
    with uow:

        card = uow.cards.get(name=cmd.name)

        if card == None:
            raise exceptions.CardNotFoundError
        if not card.check(cmd.username,cmd.password) :
            raise exceptions.UnauthorizedError


        card.update_content(cmd.content)
        uow.session.execute(
            'UPDATE cards SET content = :content WHERE name= :name',
            
            dict(name=cmd.name,content=cmd.content)
        )
        
        uow.commit()
def update_category(cmd: commands.UpdateCategory, uow: unit_of_work.AbstractUnitOfWork):
    with uow:

        card = uow.cards.get(name=cmd.name)

        if card == None:
            raise exceptions.CardNotFoundError
        if not card.check(cmd.username,cmd.password) :
            raise exceptions.UnauthorizedError


        card.update_category(cmd.category)
        uow.session.execute(
            'UPDATE cards SET category = :category WHERE name = :name',
            
            dict(name=cmd.name,category=cmd.category)
        )
       
        uow.commit()
def delete_card(cmd: commands.RemoveCard, uow: unit_of_work.AbstractUnitOfWork):
    with uow:

        card = uow.cards.get(name=cmd.name)

        if card == None:
            raise exceptions.CardNotFoundError
        if not card.check(cmd.username,cmd.password) :
            raise exceptions.UnauthorizedError

        
        uow.session.execute(
            'DELETE FROM cards '
            ' WHERE name = :name',
            dict(name=cmd.name)
        )
        
        uow.commit()


  


EVENT_HANDLERS = {
    
}

COMMAND_HANDLERS = {
    commands.CreateCard: create_card,
    commands.UpdateName: update_name,
    commands.UpdateStatus: update_status,
    commands.UpdateContent: update_content,
    commands.UpdateCategory: update_category,
    commands.RemoveCard: delete_card,
}
