class UnauthorizedError(Exception):
    def __init__(self):
        self.messages = "Unauthorized"



class DuplicatedCard(Exception):
    def __init__(self):
        self.messages = "This article's name is already used"

class CardNotFoundError(Exception):
    def __init__(self):
        self.messages = "Cannot find Card"
