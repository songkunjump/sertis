from __future__ import annotations

import dataclasses
import os
from typing import Callable, Dict, List, Union, Type, TYPE_CHECKING

from api import logging
from api.domain import commands, events

if TYPE_CHECKING:
    from . import unit_of_work


logger = logging.getLoggerForHandlers(__name__)
Message = Union[commands.Command, events.Event]


class MessageBus:
    def __init__(
        self,
        uow: unit_of_work.AbstractUnitOfWork,
        event_handlers: Dict[Type[events.Event], List[Handler]],
        command_handlers: Dict[Type[commands.Command], Handler],
    ):
        self.uow = uow
        self.event_handlers = event_handlers
        self.command_handlers = command_handlers

    def handle(self, message: Message):
        self.queue = [message]
        while self.queue:
            message = self.queue.pop(0)
            if isinstance(message, events.Event):
                try:
                    self.handle_event(message)
                except Exception:
                    raise
            elif isinstance(message, commands.Command):
                try:
                    self.handle_command(message)
                except Exception:
                    raise
            else:
                raise Exception(f"{message} was not an Event or Command")

    def handle_event(self, event: events.Event):
        handlers = self.event_handlers[type(event)]
        if handlers:
            for handler in handlers:
                try:
                    logger.info("EventHandled: %s %s", handler.__name__, event)
                    handler(event)
                    self.queue.extend(self.uow.collect_new_events())
                except Exception:
                    logger.exception("ExceptionHandlingEvent: %s", event)
                    continue
        else:
            logger.warning("EventUnhandled: %s", event)

    def handle_command(self, command: commands.Command):
        try:
            handler = self.command_handlers[type(command)]
            logger.info("CommandHandled: %s %s", handler.__name__, command)
            handler(command)
            
        except Exception as e:
            logger.exception("%s: %s", e.__class__.__name__, command)
            raise

